package com.ltmonitor.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.ltmonitor.dao.impl.DaoHibernateImpl;
import com.ltmonitor.entity.BasicData;
import com.ltmonitor.entity.PlatformState;
import com.ltmonitor.service.IBaseService;

public class BaseService extends DaoHibernateImpl implements IBaseService {
	
	/**
	 * 根据信息类型获得要发布的信息列表
	 * @param inforType
	 * @return
	 */
	public List<BasicData> getInformationByType(String inforType)
	{
		String hql = "from BasicData where parent = ? and code = ? and deleted = ?";
		
		List result = this.query(hql, new Object[]{"Information", inforType, false});
		List<BasicData> ls = new ArrayList<BasicData>();
		for(Object obj : result)
		{
			ls.add((BasicData)obj);
		}
		return ls;
	}
	
	
	/**
	 * 获得平台的各个服务器的连接状态
	 */
	public PlatformState getPlatformState()
	{
		PlatformState ps =  (PlatformState)this.find("from PlatformState");
		if(ps == null)
		{
			ps = new PlatformState();
			//this.saveOrUpdate(ps);
		}
		return ps;
	}
	
	

}
