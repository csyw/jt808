package com.ltmonitor.service;

import com.ltmonitor.dao.IBaseDao;
import com.ltmonitor.entity.Department;

public interface IDepartmentService extends IBaseDao{

	Department getDepartment(int depId);

	void saveDepartment(Department dep);

}
