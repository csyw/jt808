package com.ltmonitor.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

/**
 * GPS里程统计中间表,便于和数据进行比对计算
 * @author DELL
 *
 */

@Entity
@Table(name="GpsMileage")
@org.hibernate.annotations.Proxy(lazy = false)
@Inheritance(strategy=InheritanceType.TABLE_PER_CLASS) 
public class GpsMileage extends TenantEntity
{
	public GpsMileage()
	{
		setCreateDate(new java.util.Date());
		setLastCompTime(new java.util.Date());
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private int entityId;
	public  int getEntityId() {
		return entityId;
	}
	public  void setEntityId(int value) {
		entityId = value;
	}
	
	
	
	private String plateNo;
	public final String getPlateNo()
	{
		return plateNo;
	}
	public final void setPlateNo(String value)
	{
		plateNo = value;
	}
	//行驶里程总量
	private double mileageLastHour;
	public final double getMileageLastHour()
	{
		return mileageLastHour;
	}
	public final void setMileageLastHour(double value)
	{
		mileageLastHour = value;
	}

	//油量
	private double gasLastHour;
	public final double getGasLastHour()
	{
		return gasLastHour;
	}
	public final void setGasLastHour(double value)
	{
		gasLastHour = value;
	}

	//行驶里程总量
	private double mileageLastDay;
	public final double getMileageLastDay()
	{
		return mileageLastDay;
	}
	public final void setMileageLastDay(double value)
	{
		mileageLastDay = value;
	}

	//油量
	private double gasLastDay;
	public final double getGasLastDay()
	{
		return gasLastDay;
	}
	public final void setGasLastDay(double value)
	{
		gasLastDay = value;
	}

	//行驶里程总量
	private double mileageLastMonth;
	public final double getMileageLastMonth()
	{
		return mileageLastMonth;
	}
	public final void setMileageLastMonth(double value)
	{
		mileageLastMonth = value;
	}

	//油量
	private double gasLastMonth;
	public final double getGasLastMonth()
	{
		return gasLastMonth;
	}
	public final void setGasLastMonth(double value)
	{
		gasLastMonth = value;
	}

	//上次比较过的油量，一般是五分钟比较一次，可以自己设定比较的间隔分钟
	private double gasLastComp;
	public final double getGasLastComp()
	{
		return gasLastComp;
	}
	public final void setGasLastComp(double value)
	{
		gasLastComp = value;
	}
	//上次比较的里程数
	private double mileageLastComp;
	public final double getMileageLastComp()
	{
		return mileageLastComp;
	}
	public final void setMileageLastComp(double value)
	{
		mileageLastComp = value;
	}

	//上次比较时间
	private java.util.Date lastCompTime = new java.util.Date(0);
	public final java.util.Date getLastCompTime()
	{
		return lastCompTime;
	}
	public final void setLastCompTime(java.util.Date value)
	{
		lastCompTime = value;
	}
}