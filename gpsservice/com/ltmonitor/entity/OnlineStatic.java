﻿package com.ltmonitor.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

//上线率统计

@Entity
@Table(name="onlineStatic")
@org.hibernate.annotations.Proxy(lazy = false)
@Inheritance(strategy=InheritanceType.TABLE_PER_CLASS) 
public class OnlineStatic extends TenantEntity 
{
	//统计类型，便于根据不同的类型，生成不同的图标
	public static final int BY_HOUR = 0;
	public static final int BY_MINUTE = 1; //每五分钟一次，或30分钟一次.
	public static final int BY_DAY = 2;
	
	public OnlineStatic()
	{
		this.setCreateDate(new Date());
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private int entityId;
	public  int getEntityId() {
		return entityId;
	}
	public  void setEntityId(int value) {
		entityId = value;
	}
	
	private int depId;
	public final int getDepId()
	{
		return depId;
	}
	public final void setDepId(int value)
	{
		depId = value;
	}

	private String depName;
	public final String getDepName()
	{
		return depName;
	}
	public final void setDepName(String value)
	{
		depName = value;
	}

	private int vehicleNum;
	public final int getVehicleNum()
	{
		return vehicleNum;
	}
	public final void setVehicleNum(int value)
	{
		vehicleNum = value;
	}

	private int onlineNum;
	public final int getOnlineNum()
	{
		return onlineNum;
	}
	public final void setOnlineNum(int value)
	{
		onlineNum = value;
	}

	private double onlineRate;
	public final double getOnlineRate()
	{
		return onlineRate;
	}
	public final void setOnlineRate(double value)
	{
		onlineRate = value;
	}

	private java.util.Date statisticDate = new java.util.Date(0);

	private int parentDepId;
	public final int getParentDepId()
	{
		return parentDepId;
	}
	public final void setParentDepId(int value)
	{
		parentDepId = value;
	}

	private int intervalType;
	public java.util.Date getStatisticDate() {
		return statisticDate;
	}
	public void setStatisticDate(java.util.Date statisticDate) {
		this.statisticDate = statisticDate;
	}
	public int getIntervalType() {
		return intervalType;
	}
	public void setIntervalType(int intervalType) {
		this.intervalType = intervalType;
	}
}