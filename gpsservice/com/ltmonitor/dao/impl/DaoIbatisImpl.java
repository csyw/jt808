package com.ltmonitor.dao.impl;

import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.orm.ibatis.SqlMapClientCallback;

import com.ibatis.sqlmap.client.PageResult;
import com.ibatis.sqlmap.client.SqlMapExecutor;
import com.ibatis.sqlmap.client.spring.CustomIbatisDaoSupport;

public class DaoIbatisImpl extends CustomIbatisDaoSupport {

	private static Logger logger = Logger.getLogger(DaoIbatisImpl.class);

	public void batchInsert(final String insertStatementId, final List list) {
		getSqlMapClientTemplate().execute(new SqlMapClientCallback() {
			public Object doInSqlMapClient(SqlMapExecutor executor)
					throws SQLException {
				executor.startBatch();
				// do some iBatis operations here
				for (int i = 0, count = list.size(); i < count; i++) {
					executor.insert(insertStatementId, list.get(i));
					if (i % 50 == 0) {
						System.out.println("----" + i);// 没有意义只为测试
					}
				}
				executor.executeBatch();
				return null;
			}
		});
	}

	public void batchUpdate(final String statementName, final List list) {
		try {
			if (list != null) {
				this.getSqlMapClientTemplate().execute(
						new SqlMapClientCallback() {
							public Object doInSqlMapClient(
									SqlMapExecutor executor)
									throws SQLException {
								executor.startBatch();
								for (int i = 0, n = list.size(); i < n; i++) {
									executor.update(statementName, list.get(i));
									
									if(i % 50 == 0 && i > 0)
									{
										executor.executeBatch();
										executor.startBatch();									
									}
								}
								executor.executeBatch();
								return null;
							}
						});
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}

	}
	
	public void update(final String statementName, HashMap paramMap)
	{
		getSqlMapClientTemplate().update(statementName, paramMap);
	}
	/**
	 private static final int DB_BATCH_SIZE = 1000;

	    public void storeMyData(final List<MyData> listData)
	    {
	        getSqlMapClientTemplate().execute( new SqlMapClientCallback()
	        {
	            @Override
	            public Object doInSqlMapClient(SqlMapExecutor executor) throws SQLException
	            {
	                int count = 0, total = 0;

	                Map<String, Object> params = new HashMap<String, Object>();

	                executor.startBatch();

	                for (MyData data: listData)
	                {
	                    params.put("param name 1", data.getValue());

	                    executor.insert("insertData", params);

	                    count++;
	                    if (count % DB_BATCH_SIZE == 0) 
	                    {
	                        total += executor.executeBatch();
	                        executor.startBatch();
	                    }

	                    params.clear();
	                }

	                total += executor.executeBatch();

	                return new Integer(total);
	            }
	        });
	    }
*/
	/**
	 * 分页查询
	 * 
	 * @param id
	 * @param params
	 *            查询参数，里面必须要有一个pageNo的页号参�?
	 * @param pageSize
	 * @return
	 */
	public PageResult queryByPagination(String id, Map params, int pageNo,
			int pageSize) {
		int start = (pageNo - 1) * pageSize;
		return getSqlMapClientTemplate().queryForPageResult(id, params, pageNo,
				pageSize);
	}

	/**
	 * 不分页查询
	 * 
	 * @param id
	 *            查询ID
	 * @param params
	 *            查询参数
	 * @return
	 */
	public List queryForList(String id, Map params) {
		return super.getSqlMapClientTemplate().queryForList(id, params);
	}

	public String callProcedure() {
		Date date1 = new Date();

		Date date2 = new Date();

		Map parameterObject = new HashMap();
		parameterObject.put("startDate", "2008-08-11");
		parameterObject.put("endDate", "2008-08-15");
		parameterObject.put("queryStr", " modality = 'CT' and prop_1 = '408' ");
		// parameterObject.put("dynamicID", new Integer(4));
		getSqlMapClientTemplate().queryForObject("calculateApplyTime",
				parameterObject);

		Object obj = parameterObject.get("dynamicID");

		return obj != null ? obj.toString() : null;
	}

}
